# Gallium
Gallium is a static site generator with support for handlebars templates and markdown

Gallium can be installed as a CLI application from [crates.io](https://crates.io/crates/gallium) with the following command: `cargo install gallium`

Gallium is also available on [Docker Hub](https://hub.docker.com/r/pythondude/gallium) at `pythondude/gallium:<version>`

## Gitlab Pages
Gallium was designed to be used in Gitlab Pages. Here is an example `.gitlab-ci.yml`:

```yml
image: pythondude/gallium:<version>

pages:
  stage: deploy
  script:
  - gallium --input root --templates templates --output public
  artifacts:
    paths:
    - public
  only:
  - master
```
You can see Gallium in action on Gitlab pages [here](https://gitlab.com/pythondude325/static-site-test).

## Todo
- Make watcher option a feature so it can be excluded from the docker image
- Write an HTTP file server library/binary combo with support for live-reloading. Websocket lib: websocket, tungstenite. Live reload script: https://github.com/tapio/live-server/blob/master/injected.html
- Write tests
- Write CI config for automatic crates.io and docker hub publishing

