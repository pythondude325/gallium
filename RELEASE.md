# Steps for release

Increment version in Cargo.toml

## Git tag
```sh
git tag -a "v${version}" -m "version ${version}"
git push --follow-tags
```

## Docker Publish
```sh
./docker_build.sh ${version}
```

