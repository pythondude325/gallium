#!/bin/bash

version="$1"

if [ -z "${version}" ]; then
    echo "Usage: $0 <version>"
    exit 1
fi

docker_repo="pythondude/gallium"

docker build -t "${docker_repo}" .

docker tag "${docker_repo}" "${docker_repo}:latest"
docker push "${docker_repo}:latest"

docker tag "${docker_repo}:latest" "${docker_repo}:${version}"
docker push "${docker_repo}:${version}"

docker image rm "${docker_repo}:latest" "${docker_repo}:${version}"