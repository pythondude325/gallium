use std::hash::{Hasher};
use std::path::PathBuf;
use std::fs;
use std::io;

use failure::Fail;

fn hash(value: impl std::hash::Hash) -> u64 {
    let mut hasher = std::collections::hash_map::DefaultHasher::new();
    value.hash(&mut hasher);
    hasher.finish()
}

#[derive(Debug, Fail)]
pub(crate) enum FileCacheError {
    #[fail(display = "IO Error: {}", error)]
    IO { error: io::Error },

    #[fail(display = "HTTP Error: {}", error)]
    HTTP { error: reqwest::Error }
}

impl From<io::Error> for FileCacheError {
    fn from(error: io::Error) -> FileCacheError {
        FileCacheError::IO { error }
    }
}

impl From<reqwest::Error> for FileCacheError {
    fn from(error: reqwest::Error) -> FileCacheError {
        FileCacheError::HTTP { error }
    }
}

pub(crate) struct FileCache {
    dir: tempfile::TempDir
}

impl FileCache {
    pub fn new() -> Result<FileCache, FileCacheError> {
        Ok(FileCache {
            dir: tempfile::tempdir()?
        })
    }

    pub fn get_file(&self, url: &str) -> Result<PathBuf, FileCacheError> {
        let url_hash = format!("{:x}", hash(url));

        let file_path = self.dir.as_ref().join(url_hash);

        if file_path.exists() {
            println!("FileCache: {} was cached at {}", url, file_path.to_str().unwrap_or(""));
            Ok(file_path)
        } else {
            let mut file = fs::File::create(&file_path)?;

            reqwest::get(url)?.error_for_status()?.copy_to(&mut file)?;

            println!("FileCache: downloading {} to {}", url, file_path.to_str().unwrap_or(""));
            Ok(file_path)
        }
    }
}
