use std::io::Result;
use std::path::PathBuf;

use handlebars::Handlebars;
use structopt::StructOpt;

mod file_cache;
mod markdown_helper;
mod render;
mod watch;

#[derive(Debug, StructOpt)]
struct Opt {
    #[structopt(short = "i", long = "input")]
    /// Sets the input directory
    input_dir: PathBuf,

    #[structopt(short = "t", long = "templates")]
    /// Sets the template directory
    template_dir: PathBuf,

    #[structopt(short = "o", long = "output")]
    /// Sets the output directory
    output_dir: PathBuf,

    #[structopt(short = "w", long = "watch")]
    /// Watch the input and template directories for changes
    watch: bool,
}

fn main() -> Result<()> {
    let opt = Opt::from_args();

    if !opt.template_dir.is_dir() {
        eprintln!("Template dir must be a directory.");
        return Ok(());
    }

    if !opt.input_dir.is_dir() {
        eprintln!("Input must be a directory.");
        return Ok(());
    }

    if opt.output_dir.exists() && !opt.output_dir.is_dir() {
        eprintln!("Output must be a directory or not exist.");
        return Ok(());
    }

    let render_paths = render::RendererPaths::new(opt.input_dir, opt.template_dir, opt.output_dir);

    if opt.watch {
        // Watch for file changes

        watch::watch_files(&render_paths);
    } else {
        // Default behavior

        let mut handlebars = Handlebars::new();
        handlebars.register_helper("markdown", Box::new(crate::markdown_helper::markdown_block));

        render::render_site(&render_paths, &mut handlebars).unwrap();
    }

    Ok(())
}
