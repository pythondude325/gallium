use handlebars::*;
use pulldown_cmark::{html, Options, Parser};

pub fn markdown_block<'reg, 'rc>(
    h: &Helper<'reg, 'rc>,
    r: &'reg Handlebars,
    ctx: &Context,
    rc: &mut RenderContext<'reg>,
    out: &mut dyn Output,
) -> HelperResult {
    if let Some(inner_template) = h.template() {
        let contents = inner_template.renders(r, ctx, rc)?;

        let dedented_contents = textwrap::dedent(&contents);

        let mut options = Options::empty();
        options.insert(Options::ENABLE_STRIKETHROUGH);
        let parser = Parser::new_ext(&dedented_contents, options);

        let mut html_output = String::new();
        html::push_html(&mut html_output, parser);

        out.write(&html_output)?;

        Ok(())
    } else {
        Ok(())
    }
}
