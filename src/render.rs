use std::collections::HashMap;
use std::fs;
use std::io;
use std::path::{Path, PathBuf};

use handlebars as hb;
use walkdir::WalkDir;
use failure::Fail;

pub struct RendererPaths {
    input_dir: PathBuf,
    template_dir: PathBuf,
    output_dir: PathBuf,
    file_cache: crate::file_cache::FileCache
}

impl RendererPaths {
    pub fn new<P: AsRef<Path>>(input_dir: P, template_dir: P, output_dir: P) -> RendererPaths {
        RendererPaths {
            input_dir: input_dir.as_ref().to_path_buf(),
            template_dir: template_dir.as_ref().to_path_buf(),
            output_dir: output_dir.as_ref().to_path_buf(),
            file_cache: crate::file_cache::FileCache::new().unwrap() // TODO: Repace this with proper error handleing
        }
    }

    pub fn get_refs(&self) -> (&Path, &Path, &Path) {
        (&self.input_dir, &self.template_dir, &self.output_dir)
    }
}

#[derive(Debug, Fail)]
pub enum RenderError {
    #[fail(display = "IO Error: {}", error)]
    IO { error: io::Error },

    #[fail(display = "Template Error: {}", error)]
    Template { error: hb::TemplateFileError },

    #[fail(display = "Render Error: {}", error)]
    Render { error: hb::TemplateRenderError },

    #[fail(display = "TOML Derserialization Error in {}: {}", path, error)]
    Toml { path: String, error: toml::de::Error },

    #[fail(display = "HTTP Error from: {}", error)]
    HTTP { error: reqwest::Error }
}

impl From<io::Error> for RenderError {
    fn from(error: io::Error) -> RenderError {
        RenderError::IO { error }
    }
}

impl From<hb::TemplateFileError> for RenderError {
    fn from(error: hb::TemplateFileError) -> RenderError {
        RenderError::Template { error }
    }
}

use crate::file_cache::FileCacheError;
impl From<FileCacheError> for RenderError {
    fn from(error: FileCacheError) -> RenderError {
        match error {
            FileCacheError::IO { error } => RenderError::IO { error },
            FileCacheError::HTTP { error } => RenderError::HTTP { error }
        }
    }
}

impl RenderError {
    fn from_template_render_error(mut error: hb::TemplateRenderError, path: &Path) -> RenderError {
        if let hb::TemplateRenderError::TemplateError(ref mut template_error) = error {
            template_error.template_name = Some(path.to_string_lossy().into_owned());
        } else if let hb::TemplateRenderError::RenderError(ref mut render_error) = error {
            render_error.template_name = Some(path.to_string_lossy().into_owned());
        }

        RenderError::Render { error }
    }

    fn from_toml_error(error: toml::de::Error, path: &Path) -> RenderError {
        RenderError::Toml { path: path.to_str().unwrap_or("").to_string(), error }
    }
}

pub fn render_file(
    entry_path: impl AsRef<Path>,
    paths: &RendererPaths,
    template_registry: &hb::Handlebars,
) -> Result<(), RenderError> {
    let (input_dir, _template_dir, output_dir) = paths.get_refs();

    let entry_path = entry_path.as_ref();
    let stripped_path = entry_path
        .strip_prefix(input_dir)
        .expect("File was not in directory");

    if entry_path.is_dir() {
        let destination_path = output_dir.join(stripped_path);

        fs::create_dir(destination_path)?;
    } else {
        // Entry is a file

        match entry_path.extension().map(|s| s.to_str()).unwrap_or(None) {
            Some("hbs") => {
                let destination_path = output_dir.join(stripped_path.with_extension("html"));

                let template_data = HashMap::<String, String, _>::new();
                // TODO: Put the url path in the template data.

                template_registry
                    .render_template_source_to_write(
                        &mut fs::File::open(entry_path)?,
                        &template_data,
                        fs::File::create(destination_path)?,
                    )
                    .map_err(|e| RenderError::from_template_render_error(e, entry_path))?;
            }
            Some("ga") => {
                #[derive(serde::Deserialize)]
                enum GalliumFileConfig {
                    #[serde(rename = "download_file")]
                    DownloadFile { url: String, output: String }

                    // TODO: Add download_zip option
                }

                let file_contents = std::fs::read_to_string(entry_path)?;
                let gallium_file_config = toml::from_str(&file_contents).map_err(|e| RenderError::from_toml_error(e, entry_path))?;

                match gallium_file_config {
                    GalliumFileConfig::DownloadFile{ url, output } => {
                        println!("Downloading {}", url);

                        let downloaded_file = paths.file_cache.get_file(&url)?;
                        let destination_path = output_dir.join(output);

                        fs::copy(downloaded_file, destination_path)?;
                    }
                }
            }
            Some(_) | None => {
                let destination_path = output_dir.join(stripped_path);

                fs::copy(entry_path, destination_path)?;
            }
        }
    }

    Ok(())
}

pub fn render_template(
    template_path: impl AsRef<Path>,
    paths: &RendererPaths,
    template_registry: &mut hb::Handlebars,
) -> Result<(), RenderError> {
    let (_, template_dir, _) = paths.get_refs();

    let path = template_path.as_ref();

    let stripped_path = path
        .strip_prefix(template_dir)
        .expect("Unexpected template");

    let template_name = stripped_path.with_extension("");

    template_registry.register_template_source(
        &template_name.to_string_lossy(),
        &mut fs::File::open(&path)?,
    )?;

    Ok(())
}

pub fn render_site(paths: &RendererPaths, handlebars: &mut hb::Handlebars) -> Result<(), RenderError> {
    let (input_dir, template_dir, output_dir) = paths.get_refs();

    if output_dir.exists() {
        fs::remove_dir_all(output_dir)?;
    }

    for entry in WalkDir::new(template_dir) {
        let entry = entry.map_err(io::Error::from)?;
        let entry_path = entry.path();

        if entry_path.is_file() && Some(std::ffi::OsStr::new("hbs")) == entry_path.extension() {
            let result = render_template(entry_path, &paths, handlebars);

            if let Err(error) = result {
                eprintln!("{}", error);
            }
        }
    }

    for entry in WalkDir::new(input_dir) {
        let entry = entry.map_err(io::Error::from)?;

        let result = render_file(entry.path(), &paths, &handlebars);

        if let Err(error) = result {
            eprintln!("{}", error);
        }
    }

    Ok(())
}
